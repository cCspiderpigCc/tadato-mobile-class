//
//  TodoFirebase.swift
//  project
//
//  Created by Admin on 15/03/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import Foundation
import Firebase

class TodoFirebase {
    
    static func addTodo(todo:Todo){
        let ref = FIRDatabase.database().reference().child("todos").child(todo.id)
        ref.setValue(todo.toJson())
    }
    
    static func getTodos(lastUpdateTime: Date?, callback:@escaping ([Todo])->Void){
        let handler = {(snapshot:FIRDataSnapshot) in
            var todos = [Todo]()
            for child in snapshot.children.allObjects{
                if let childData = child as? FIRDataSnapshot{
                    if let json = childData.value as? Dictionary<String,Any>{
                        let st = Todo(json: json)
                        todos.append(st)
                    }
                }
            }
            callback(todos)
        };
        
        let ref = FIRDatabase.database().reference().child("todos")
        if (lastUpdateTime != nil) {
            let fbQuery = ref.queryOrdered(byChild: "lastUpdateTime")
                             .queryStarting(atValue: lastUpdateTime!.toFirebase())
            fbQuery.observe(FIRDataEventType.value, with: handler)
        } else {
            ref.observe(FIRDataEventType.value, with: handler)
        }
    }
    
    static func markAsDone(todo:Todo, callback:@escaping (Bool)->Void) {
        let ref = FIRDatabase.database().reference().child("todos").child(todo.id)
        let updateData = ["done" : true,
                          "lastUpdateTime" : FIRServerValue.timestamp()] as [String : Any];
        
        ref.updateChildValues(updateData) { (error, resRef) in
            if (error == nil) {
                callback(true)
            } else {
                callback(false)
            }
        }
    }
}
