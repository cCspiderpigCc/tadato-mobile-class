//
//  LocationViewerController.swift
//  project
//
//  Created by Admin on 18/03/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import UIKit
import GoogleMaps

class LocationViewController: UITableViewController, CLLocationManagerDelegate {
    let locationManager = CLLocationManager()
    ;
    var didCameraLoad = false;
    private var longitude:Double = 0.0
    private var latitude:Double = 0.0
    
    override func loadView() {
        didCameraLoad = false;
        
        let camera = GMSCameraPosition.camera(withLatitude: self.latitude, longitude: self.longitude, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
        marker.title = "The todo is HERE!"
        marker.map = mapView
        
        view = mapView
    }
    
    public func setTodoPos(longitude: Double, latitude: Double) {
        self.longitude = longitude
        self.latitude = latitude
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (didCameraLoad == false) {
            let locValue:CLLocationCoordinate2D = manager.location!.coordinate
            print("locations = \(locValue.latitude) \(locValue.longitude)")
            
            let mapView = view as! GMSMapView
                        
            // Creates a marker in the center of the map.
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude)
            marker.title = "You're here!"
            marker.icon = GMSMarker.markerImage(with: .blue)
            marker.map = mapView
            
            view = mapView
        }
        didCameraLoad = true;
    }
}
