//
//  TodoDetailsController.swift
//  project
//
//  Created by Tal Gvili on 28/01/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import UIKit

class TodoDetailsController: UIViewController {

    @IBOutlet weak var completeSpinner: UIActivityIndicatorView!
    @IBOutlet weak var todoTitle: UILabel!
    @IBOutlet weak var todoDescription: UITextView!
    @IBOutlet weak var todoAsker: UILabel!
    @IBOutlet weak var todoStatus: UILabel!
    @IBOutlet weak var completeLabel: UILabel!
    @IBOutlet weak var ownerPic: UIImageView!
    @IBOutlet weak var loadingOwnerPic: UIActivityIndicatorView!
    @IBOutlet weak var completeBtn: UIButton!
    
    private var markerView: LocationViewController!
    
    var todo:Todo!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.todoTitle.text =  todo.title;
        self.todoDescription.text = todo.description;
        
        if (todo.done) {
            self.todoStatus.text = "Done!"
            self.completeBtn.isHidden = true
        } else {
            self.todoStatus.text = "Do me"
        }
        
        Model.instance.userService?.getUserById(id: todo.owner, callback: { (owner) in
            self.todoAsker.text = owner.name
            
            Model.instance.userService?.getImage(urlStr: owner.profilePicUrl!, callback: { (image) in
                self.ownerPic!.image = image
                self.loadingOwnerPic.stopAnimating();
            })
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? LocationViewController  {
            vc.setTodoPos(longitude: self.todo.longitude, latitude: self.todo.latitude)
            self.markerView = vc
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onCompleteClick(_ sender: AnyObject) {
        completeSpinner.startAnimating()
        completeBtn.isHidden = true
        
        Model.instance.todoService?.markAsDone(todo: todo, callback: { (success) in
            if (success) {
                self.todoStatus.text = "Done!"
                self.completeSpinner.stopAnimating()
            } else {
                self.completeLabel.isHidden = false
                self.completeSpinner.stopAnimating()
                self.completeBtn.isHidden = false
            }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
