//
//  User.swift
//  HelpMe
//

import Foundation



class User {
    var id:String
    var profilePicUrl: String?
    var name:String
    
    init(id:String, name:String, profilePicUrl:String? = nil){
        self.id = id
        self.name = name
        self.profilePicUrl = profilePicUrl
    }
    
    init(json:Dictionary<String, Any>){
        id = json["id"] as! String
        name = json["name"] as! String
        
        if let im = json["profilePicUrl"] as? String{
            profilePicUrl = im
        }
    }
    
    func toJson() -> Dictionary<String,String> {
        var json = Dictionary<String,String>()
        json["id"] = id
        json["name"] = name
        if (profilePicUrl != nil){
            json["profilePicUrl"] = profilePicUrl!
        }
        
        return json
    }
    
    
}
