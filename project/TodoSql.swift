//
//  TodoSql.swift
//  project
//
//  Created by Admin on 16/03/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import Foundation

class TodoSql {
    static let TODO_TABLE = "TODOS"
    static let TODO_ID = "TODO_ID"
    static let TODO_TITLE = "NAME"
    static let TODO_DESC = "DESCRIPTION"
    static let TODO_OWNER = "OWNER"
    static let TODO_DONE = "DONE"
    static let TODO_LON = "LONGITUDE"
    static let TODO_LAT = "LATIDTUDE"
    static let TODO_LAST_UPDATE = "TODO_LAST_UPDATE"
    
    static func createTable(database:OpaquePointer?)->Bool {
        var errormsg: UnsafeMutablePointer<Int8>? = nil
        
        let res = sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS " + TODO_TABLE + " ( " + TODO_ID + " TEXT PRIMARY KEY, "
            + TODO_TITLE + " TEXT, "
            + TODO_DESC + " TEXT, "
            + TODO_OWNER + " TEXT, "
            + TODO_DONE + " BOOLEAN, "
            + TODO_LON + " DOUBLE, "
            + TODO_LAT + " DOUBLE, "
            + TODO_LAST_UPDATE + " DOUBLE)", nil, nil, &errormsg);
        if(res != 0){
            print("error creating table");
            return false
        }
        
        return true
    }
    
    static func addOrUpdateTodoToLocalDB(database:OpaquePointer?, todo:Todo) {
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO " + TodoSql.TODO_TABLE
            + "(" + TodoSql.TODO_ID + ","
            + TodoSql.TODO_TITLE + ","
            + TodoSql.TODO_DESC + ","
            + TodoSql.TODO_OWNER + ","
            + TodoSql.TODO_DONE + ","
            + TodoSql.TODO_LON + ","
            + TodoSql.TODO_LAT + ","
            + TodoSql.TODO_LAST_UPDATE + ") VALUES (?,?,?,?,?,?,?,?);",-1, &sqlite3_stmt,nil) == SQLITE_OK){
            
            let id = todo.id.cString(using: .utf8)
            let title = todo.title.cString(using: .utf8)
            let desc = todo.description.cString(using: .utf8)
            let owner = todo.owner.cString(using: .utf8)
            
            
            sqlite3_bind_text(sqlite3_stmt, 1, id,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, title,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, desc,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 4, owner,-1,nil);
            
            var done = 0;
            if(todo.done) {
                done = 1
            }
            
            sqlite3_bind_int(sqlite3_stmt, 5, Int32(done))
            sqlite3_bind_double(sqlite3_stmt, 6, todo.longitude)
            sqlite3_bind_double(sqlite3_stmt, 7, todo.latitude)
            
            var lastUpdate = todo.lastUpdateTime
            if (lastUpdate == nil){
                lastUpdate = Date()
            }
            sqlite3_bind_double(sqlite3_stmt, 8, lastUpdate!.toFirebase());
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new row added succefully")
            }
        }
        sqlite3_finalize(sqlite3_stmt)
    }
    
    static func getAllTodosFromLocalDb(database:OpaquePointer?)->[Todo]{
        var todos = [Todo]()
        
        var sqlite3_stmt: OpaquePointer? = nil
        
        if (sqlite3_prepare_v2(database,"SELECT " + TodoSql.TODO_ID + ","
            + TodoSql.TODO_TITLE + ","
            + TodoSql.TODO_DESC + ","
            + TodoSql.TODO_OWNER + ","
            + TodoSql.TODO_DONE + ","
            + TodoSql.TODO_LON + ","
            + TodoSql.TODO_LAT + " from " + TODO_TABLE + "; ",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id =  String(validatingUTF8:sqlite3_column_text(sqlite3_stmt,0))
                let title =  String(validatingUTF8:sqlite3_column_text(sqlite3_stmt,1))
                let desc =  String(validatingUTF8:sqlite3_column_text(sqlite3_stmt,2))
                let owner =  String(validatingUTF8:sqlite3_column_text(sqlite3_stmt,3))
                let doneVal =  Int(sqlite3_column_int(sqlite3_stmt, 4))
                let lon =  Double(sqlite3_column_double(sqlite3_stmt,5))
                let lat =  Double(sqlite3_column_double(sqlite3_stmt,6))
                //let update =  Double(sqlite3_column_double(sqlite3_stmt,8))
                print("read from filter todo: id\(id) title\(title) desc\(desc) owner\(owner) done?\(doneVal) lon\(lon) lat\(lat)")
                
                var done = false;
                if (doneVal == 1) {
                    done = true;
                }
                let todo = Todo(id: id!, latitude: lat, longitude: lon, title: title!, description: desc!, owner: owner!, done: done)
                todos.append(todo)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return todos
    }
}

extension String {
    public init?(validatingUTF8 cString: UnsafePointer<UInt8>) {
        if let (result, _) = String.decodeCString(cString, as: UTF8.self,
                                                  repairingInvalidCodeUnits: false) {
            self = result
        }
        else {
            return nil
        }
    }
}
    
