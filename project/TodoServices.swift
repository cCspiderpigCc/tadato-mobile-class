//
//  TodoServices.swift
//  project
//
//  Created by Admin on 15/03/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import Foundation

let notifyTodosListUpdate = "com.tadato.NotifyTodosListUpdate"

class TodoServices {
    
    init() {
    }
    
    func addTodo(todo:Todo) {
        return TodoFirebase.addTodo(todo: todo);
    }
    
    func getTodosAndNotify() {
        // get last update date from SQL
        let db = Model.instance.modelSql?.database;
        let lastUpdateDate = LastUpdateTable.getLastUpdateDate(database: db, table: TodoSql.TODO_TABLE)
        
        TodoFirebase.getTodos(lastUpdateTime: lastUpdateDate, callback: { (todos) in
            print("found \(todos.count) new todos")
            var lastUpdateTime:Date?
            
            // add todo to local db and find latest lastUpdateTime
            for todo in todos {
                TodoSql.addOrUpdateTodoToLocalDB(database: db, todo: todo)
                
                if lastUpdateTime == nil {
                    lastUpdateTime = todo.lastUpdateTime
                } else {
                    if lastUpdateTime!.compare(todo.lastUpdateTime!) == ComparisonResult.orderedAscending{
                        lastUpdateTime = todo.lastUpdateTime
                    }
                }
            }
            
            //update the last update table
            if (lastUpdateTime != nil) {
                LastUpdateTable.setLastUpdate(database: db, table: TodoSql.TODO_TABLE, lastUpdate: lastUpdateTime!)
            }
            
            let totalTodos = TodoSql.getAllTodosFromLocalDb(database: db)
            
            //return the list to the observers using notification center
            NotificationCenter.default.post(name: Notification.Name(rawValue:
                notifyTodosListUpdate), object:nil , userInfo:["todos":totalTodos])
        })
    }
    
    func markAsDone(todo:Todo, callback:@escaping (Bool)->Void) {
        return TodoFirebase.markAsDone(todo: todo, callback: callback);
    }
}
