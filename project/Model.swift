//
//  Model.swift
//  project
//
//  Created by Admin on 28/01/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import Foundation
import Firebase

class Model {
    static let instance = Model()
    
    lazy public var modelSql:ModelSql? = ModelSql()
    private var modelFirebase:ModelFirebase? = ModelFirebase()
    
    // Services
    public var userService:UserServices? = UserServices()
    
    public var authService:Auth? = Auth()
    
    public var todoService:TodoServices? = TodoServices()
    
    private init(){
    }
}

extension Date {
    
    func toFirebase()->Double{
        return self.timeIntervalSince1970 * 1000
    }
    
    static func fromFirebase(_ interval:String)->Date{
        return Date(timeIntervalSince1970: Double(interval)!)
    }
    
    static func fromFirebase(_ interval:Double)->Date{
        if (interval>9999999999){
            return Date(timeIntervalSince1970: interval/1000)
        }else{
            return Date(timeIntervalSince1970: interval)
        }
    }
    
    var stringValue: String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.string(from: self)
    }
    
}
