//
//  LocationInputController.swift
//  project
//
//  Created by Daniel Jakobsen, Tom Chen on 28/01/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import UIKit
import GoogleMaps

class LocationInputController: UITableViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    let locationManager = CLLocationManager()
    let marker = GMSMarker();
    var didCameraLoad = false;
    
    override func loadView() {
        didCameraLoad = false;
        
        let camera = GMSCameraPosition.camera(withLatitude: 89, longitude: 123, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.delegate = self
        
        view = mapView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        marker.title = "Your todo!"
        marker.map = view as! GMSMapView?
        marker.position.latitude = coordinate.latitude;
        marker.position.longitude = coordinate.longitude;
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (didCameraLoad == false) {
            let locValue:CLLocationCoordinate2D = manager.location!.coordinate
            print("locations = \(locValue.latitude) \(locValue.longitude)")
            
            let mapView = view as! GMSMapView
            let camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: 6.0)
            
            mapView.camera = camera;
            
            // Creates a marker in the center of the map.
            marker.position = CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude)
            marker.title = "Your todo!"
            marker.map = mapView
            
            view = mapView
        }
        didCameraLoad = true;
    }
    
    func getMarkerLocation()-> CLLocationCoordinate2D {
        return self.marker.position;
    }
}
