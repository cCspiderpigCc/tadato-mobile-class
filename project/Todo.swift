//
//  Todo.swift
//  HelpMe
//

import Foundation
import Firebase

class Todo {
    var id:String
    var latitude: Double
    var longitude: Double
    var title:String
    var description:String
    var done:Bool
    var owner:String
    var lastUpdateTime:Date?
    
    init(id:String, latitude:Double, longitude: Double, title:String, description:String, owner: String, done: Bool) {
        self.id = id
        self.latitude = latitude
        self.longitude = longitude
        self.title = title
        self.description = description
        self.done = done
        self.owner = owner
    }
    
    init(latitude:Double, longitude: Double, title:String, description:String, owner: String){
        self.id = UUID().uuidString
        self.latitude = latitude
        self.longitude = longitude
        self.title = title
        self.description = description
        self.done = false
        self.owner = owner
    }
    
    init(json:Dictionary<String,Any>) {
        id = json["id"]! as! String
        longitude =  json["longitude"]! as! Double
        latitude = json["latitude"]! as! Double
        title = json["title"]! as! String
        description = json["description"]! as! String
        done = json["done"]! as! Bool
        owner = json["owner"]! as! String
        if let ts = json["lastUpdateTime"] as? Double {
            self.lastUpdateTime = Date.fromFirebase(ts)
        }
    }
    
    func toJson() -> Dictionary<String,Any> {
        var json = Dictionary<String,Any>()
        json["id"] = id
        json["longitude"] = longitude
        json["latitude"] = latitude
        json["owner"] = owner
        json["title"] = title
        json["description"] = description
        json["lastUpdateTime"] = FIRServerValue.timestamp()
        json["done"] = done

        return json
    }
}
