//
//  TodoListController.swift
//  project
//
//  Created by Tal Gvili on 28/01/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import UIKit

class TodoListController: UITableViewController {
    
    
    
    // Vars
    var todos = [Todo]()
    
    /// Spinner shown during load the TableView
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)

    /// View which contains the loading text and the spinner
    let loadingView = UIView()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setLoadingScreen()

        
        // Remove extra empty table cells;
        tableView.tableFooterView = UIView()

        
        NotificationCenter.default.addObserver(self, selector: #selector(self.todosListUpdated), name: NSNotification.Name(rawValue: notifyTodosListUpdate), object: nil)
        Model.instance.todoService?.getTodosAndNotify();
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return todos.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath) as! TodoCell
        
        let todo = todos[indexPath.row];
        cell.todoTitleLbl.text = todo.title;
        cell.checkMarkImg.isHidden = !todo.done;
            
        return cell
    }
    
    func todosListUpdated(notification:NSNotification){
        // If the activity subview still exists remove it.
        self.activityIndicator.stopAnimating()
        
        self.todos = notification.userInfo?["todos"] as! [Todo]
        self.tableView!.reloadData()
    }

    // Set the activity indicator into the main view
    private func setLoadingScreen() {
        
        activityIndicator.center = CGPoint(x: UIScreen.main.bounds.midX, y: UIScreen.main.bounds.midY - 40)
        activityIndicator.startAnimating()
        activityIndicator.color = UIColor.black
        self.view.addSubview(activityIndicator)

    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

//     MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//         Get the new view controller using segue.destinationViewController.
//         Pass the selected object to the new view controller.
        
        if segue.identifier == "cellToDetailsSegue" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = segue.destination as! TodoDetailsController
                controller.todo = todos[indexPath.row]
            }
        }

    }

}
