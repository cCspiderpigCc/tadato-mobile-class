//
//  ViewController.swift
//  project
//
//  Created by Daniel Jakobsen on 18/01/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailLbl: UILabel!
    
    @IBOutlet weak var errorlbl: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let preferences = UserDefaults.standard
        let email = preferences.object(forKey: "email")
        let password = preferences.object(forKey: "password")

        if email != nil && password != nil
        {
            // User cached, Hide interface and show spinner when tries to login
            
            login(email: email as! String, password: password as! String)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    @IBAction func clickLogIn(_ sender: AnyObject) {
        login(email: emailField.text!, password: passwordField.text!)
    }
    
    func login(email: String, password: String) {
        self.errorlbl.text = "";
        loading();
        
        Model.instance.authService?.login(email: email, pwd: password) { (success) in
            
            if(success == true){
                self.goToApp()
                
            } else {
                self.errorlbl.text = "Login failed."
                self.stopLoading()
            }
        }
    }

    
    func goToApp() {
        self.performSegue(withIdentifier: "goToAppSegue", sender: self)
    }
    
    func loading() {
        emailLbl.isHidden = true
        emailField.isHidden = true
        passwordLbl.isHidden = true
        passwordField.isHidden = true
        loginBtn.isHidden = true
        signUpBtn.isHidden = true
        spinner.startAnimating()
    }
    
    func stopLoading() {
        emailLbl.isHidden = false
        emailField.isHidden = false
        passwordLbl.isHidden = false
        passwordField.isHidden = false
        loginBtn.isHidden = false
        signUpBtn.isHidden = false
        spinner.stopAnimating()
    }

}

