//
//  ModelSql.swift
//  project
//
//  Created by Admin on 16/03/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import Foundation

class ModelSql {
    var database: OpaquePointer? = nil
    
    init?(){
        let dbFileName = "todo.db"
        if let dir = FileManager.default.urls(for: .documentDirectory, in:
            .userDomainMask).first{
            let path = dir.appendingPathComponent(dbFileName)
            
            if sqlite3_open(path.absoluteString, &database) != SQLITE_OK {
                print("Failed to open db file: \(path.absoluteString)")
                return nil
            }
        }
        
        if TodoSql.createTable(database: database) == false{
            return nil
        }
        
        if LastUpdateTable.createTable(database: database) == false {
            return nil
        }
    }
}
