//
//  UserFirebase.swift
//  project
//
//  Created by Admin on 28/01/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import Foundation
import Firebase

class UserFirebase {
    init() {
    }
    
    func addUser(user:User){
        let ref = FIRDatabase.database().reference().child("users").child(user.id)
        ref.setValue(user.toJson())
    }
    
    func getUserById(id:String, callback:@escaping (User)->Void){
        let ref = FIRDatabase.database().reference().child("users").child(id)
        
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            let json = snapshot.value as? Dictionary<String,Any>
            if (json != nil) {
                let st = User(json: json!)
                callback(st)
            }
        })
    }
    
    func saveImageToFirebase(image:UIImage, name:(String), callback:@escaping (String?)->Void){
        let filesRef = FIRStorage.storage().reference().child(name)
        if let data = UIImageJPEGRepresentation(image, 0.8) {
            filesRef.put(data, metadata: nil) { metadata, error in
                if (error != nil) {
                    callback(nil)
                } else {
                    let downloadURL = metadata!.downloadURL()
                    callback(downloadURL?.absoluteString)
                }
            }
        }
    }
    
    func getImageFromFirebase(url:String, callback:@escaping (UIImage?)->Void){
        let ref = FIRStorage.storage().reference(forURL: url)
        ref.data(withMaxSize: 10000000, completion: {(data, error) in
            if (error == nil && data != nil) {
                let image = UIImage(data: data!)
                callback(image)
            } else {
                callback(nil)
            }
        })
    }
}
