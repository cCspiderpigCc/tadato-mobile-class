//
//  Auth.swift
//  project
//
//  Created by Admin on 28/01/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class Auth {
    init() {
    }
    
    func register(email:String, pwd:String, profilePic:UIImage?, callback:@escaping (Bool)->Void){
        FIRAuth.auth()?.createUser(withEmail: email, password: pwd) { (user, error) in
            if error != nil{
                callback(false)
            } else {
                Model.instance.userService?.uploadImage(image: profilePic!, name: (FIRAuth.auth()?.currentUser?.uid)!, callback: {(downloadLink) in
                    
                    let newUser = User(id: (FIRAuth.auth()?.currentUser?.uid)!, name: email, profilePicUrl: downloadLink)
                    Model.instance.userService?.addUser(user: newUser)
                    self.saveToUserDefault(username: email, password: pwd)
                    callback(true)
                })
            }
        }
    }
    
    func login(email:String, pwd:String, callback:@escaping (Bool)->Void){
        FIRAuth.auth()?.signIn(withEmail: email, password: pwd) { (user, error) in
            if error != nil {
                print("\(error)")
                callback(false)
            } else {
                self.saveToUserDefault(username: email, password: pwd)
                callback(true)
            }
        }
    }
    
    func logout(){
        removeFromUserDefault();
        
        do {
            try FIRAuth.auth()?.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func isLoggedIn()->Bool{
        return (FIRAuth.auth()?.currentUser != nil)
    }
    
    func getUserId()->String {
        return (FIRAuth.auth()?.currentUser?.uid)!
    }

    
    func saveToUserDefault(username:String, password: String) {
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set(username, forKey: "email")
        userDefaults.set(password, forKey: "password")
    }
    
    func removeFromUserDefault() {
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.removeObject(forKey: "email");
        userDefaults.removeObject(forKey: "password");

    }
}
