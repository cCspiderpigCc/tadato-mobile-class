//
//  TodoCell.swift
//  project
//
//  Created by Tal Gvili on 18/03/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import UIKit

class TodoCell: UITableViewCell {

    @IBOutlet weak var todoTitleLbl: UILabel!
    @IBOutlet weak var checkMarkImg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
