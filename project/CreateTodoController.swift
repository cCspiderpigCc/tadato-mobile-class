//
//  CreateTodoController.swift
//  project
//
//  Created by Tal Gvili on 28/01/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import UIKit

class CreateTodoController: UIViewController, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var todoTitleText: UITextField!
    
    @IBOutlet weak var descriptionText: UITextView!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    private var markerView: LocationInputController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionText.text = "Describe your todo"
        todoTitleText.text = "Give it a title"
        todoTitleText.delegate = self
        descriptionText.delegate = self
        todoTitleText.textColor = UIColor.lightGray
        descriptionText.textColor = UIColor.lightGray
    }
    
    func textViewDidBeginEditing(_ descriptionText: UITextView) {
        if descriptionText.textColor == UIColor.lightGray {
            descriptionText.text = ""
            descriptionText.textColor = UIColor.black
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.textColor == UIColor.lightGray {
            textField.text = ""
            textField.textColor = UIColor.black
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? LocationInputController  {
            self.markerView = vc
        }
    }

    @IBAction func submitClick(_ sender: AnyObject) {
        if (todoTitleText.text == "" || todoTitleText.textColor == UIColor.lightGray || descriptionText.text == "" || descriptionText.textColor == UIColor.lightGray) {
            errorLabel.isHidden = false
            return;
        } else {
            let location = markerView.getMarkerLocation();
            let titleText = todoTitleText.text!;
            let descriptionText = self.descriptionText.text!;
            
            let newTodo = Todo(latitude: location.latitude, longitude: location.longitude, title: titleText, description: descriptionText, owner: (Model.instance.authService?.getUserId())!)
            Model.instance.todoService?.addTodo(todo: newTodo);
            
            self.navigationController?.popViewController(animated: true);
            return;
        }
        
    }

}
