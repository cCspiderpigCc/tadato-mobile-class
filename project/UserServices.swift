//
//  UserServices.swift
//  project
//
//  Created by Admin on 28/01/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import Foundation
import UIKit

class UserServices {
    private var userFirebase:UserFirebase? = UserFirebase()
    
    init() {
    }
    
    func addUser(user:User){
        return (userFirebase?.addUser(user: user))!;
    }
    
    func getUserById(id:String, callback:@escaping (User)->Void){
        return (userFirebase?.getUserById(id: id, callback: callback))!
    }
    
    func uploadImage(image: UIImage, name:String, callback:@escaping (String?)->Void) {
        userFirebase?.saveImageToFirebase(image: image, name: name, callback: callback);
    }
    
    func getImage(urlStr:String, callback:@escaping (UIImage?)->Void){
        // try to find the image from local cache
        let url = URL(string: urlStr)
        let localImageName = url!.lastPathComponent
        
        if let image = self.getImageFromFile(name: localImageName){
            callback(image)
        } else {
            // download image from firebase if not found
            userFirebase?.getImageFromFirebase(url: urlStr, callback: { (image) in
                if (image != nil) {
                    // cache the image
                    self.saveImageToFile(image: image!, name: localImageName)
                }
                // return the image
                callback(image)
            })
        }
    }
    
    private func saveImageToFile(image:UIImage, name:String){
        if let data = UIImageJPEGRepresentation(image, 0.8) {
            let filename = getDocumentsDirectory().appendingPathComponent(name)
            try? data.write(to: filename)
        }
    }
    
    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in:
            .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    private func getImageFromFile(name:String)->UIImage?{
        let filename = getDocumentsDirectory().appendingPathComponent(name)
        return UIImage(contentsOfFile:filename.path)
    }
}
