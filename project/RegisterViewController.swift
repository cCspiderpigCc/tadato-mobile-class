//
//  RegisterViewController.swift
//  project
//
//  Created by Tal Gvili on 15/03/2017.
//  Copyright © 2017 tadato. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var errorlbl: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet var imageView: UIImageView!
    
    let imagePicker = UIImagePickerController()
    var isImageSelected = false
    
    @IBOutlet weak var registerBtn: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(onImageClicked(_:)))
        imageView.addGestureRecognizer(tap)
        imageView.isUserInteractionEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onImageClicked(_ sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        
        /*
         The sourceType property wants a value of the enum named        UIImagePickerControllerSourceType, which gives 3 options:
         
         UIImagePickerControllerSourceType.PhotoLibrary
         UIImagePickerControllerSourceType.Camera
         UIImagePickerControllerSourceType.SavedPhotosAlbum
         
         */
        present(imagePicker, animated: true, completion: nil)

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.contentMode = .scaleAspectFit
            imageView.image = pickedImage
            isImageSelected = true
        }
        
        
        /*
         
         Swift Dictionary named “info”.
         We have to unpack it from there with a key asking for what media information we want.
         We just want the image, so that is what we ask for.  For reference, the available options are:
         
         UIImagePickerControllerMediaType
         UIImagePickerControllerOriginalImage
         UIImagePickerControllerEditedImage
         UIImagePickerControllerCropRect
         UIImagePickerControllerMediaURL
         UIImagePickerControllerReferenceURL
         UIImagePickerControllerMediaMetadata
         
         */
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }


    @IBAction func registerClicked(_ sender: AnyObject) {
        
        self.errorlbl.text = "";
        let isEmailExist = (emailField.text?.characters.count)! > 3
        let isPasswordExist = (passwordField.text?.characters.count)! > 5
        
        if(isEmailExist && isPasswordExist && isImageSelected){
            // hide interface and start spinner untill callback.
            loading()
            
            Model.instance.authService?.register(email: emailField.text!, pwd: passwordField.text!, profilePic: imageView.image) {  success in
                
                if(success == true){
                    self.goToApp()
                    
                } else {
                    self.errorlbl.text = "Registeration failed."
                    
                    // Release spinner and show interface
                    self.stopLoading()
                }
            }
        } else {
            errorlbl.text = "All fields are required."
        }
    }
    
    func goToApp() {
        self.performSegue(withIdentifier: "registerToMain", sender: self)
    }

    func loading() {
        emailLbl.isHidden = true
        emailField.isHidden = true
        passwordLbl.isHidden = true
        passwordField.isHidden = true
        imageView.isHidden = true
        registerBtn.isHidden = true
        spinner.startAnimating()
    }
    
    func stopLoading() {
        emailLbl.isHidden = false
        emailField.isHidden = false
        passwordLbl.isHidden = false
        passwordField.isHidden = false
        imageView.isHidden = false
        registerBtn.isHidden = false
        spinner.stopAnimating()
    }
}
